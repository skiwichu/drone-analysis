#!/bin/bash

set -euo pipefail

echo -e "\e[32mInstall dependecies and activate python a virtual environment\e[0m"
cd app
pip install -U pip poetry
poetry config virtualenvs.in-project true --local
poetry install -n --no-root
source .venv/bin/activate
pre-commit install
cd $(pwd)/..

echo -e "\e[32mBuild Docker containers\e[0m"
docker compose up --build -d

echo -e "\e[32mYou should see the project in your browser in several seconds\e[0m"
xdg-open 'http://localhost:8088' &

echo -e "\e[32mDone\e[0m"
exit 0
