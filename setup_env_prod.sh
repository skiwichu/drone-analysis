#!/bin/sh

set -e

echo DEBUG=0 >> .env.prod
echo SECRET_KEY=$SECRET_KEY >> .env.prod
echo DJANGO_ALLOWED_HOSTS=$HOST >> .env.prod
echo SENTRY_DSN=$SENTRY_DSN >> .env.prod
