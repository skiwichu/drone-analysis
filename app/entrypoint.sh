#!/bin/bash

set -euo pipefail

# Django start-up commands
echo "Checking new migrations"
python manage.py migrate --no-input
echo "Collecting static files for Whitenoise"
python manage.py collectstatic --no-input

echo "Ready to start"
exec "$@"
