import abc
import sys
import uuid
from collections.abc import Iterator
from datetime import datetime
from decimal import Decimal
from typing import Any, NamedTuple

from django.core.exceptions import ValidationError
from django.core.management.base import OutputWrapper
from django.db.models import Model

from .exceptions import DuplicityFlightError, ImproperlyConfiguredFile
from .importers import InputFileGenerator
from .models import Flight


class DataInserter(abc.ABC):
    BATCH_SIZE = 1000

    def __init__(
        self, path: str, schema: type[NamedTuple], file_generator: type[InputFileGenerator], model: type[Model]
    ) -> None:
        self.path = path
        self.schema = schema
        self.file_generator = file_generator(path, schema)
        self.model = model
        self.stdout = OutputWrapper(sys.stdout)
        self.stderr = OutputWrapper(sys.stderr)

    def process(self) -> None:
        iterator = self.file_generator.iterate_file()
        self.insert_data(iterator)

    def insert_data(self, iterator: Iterator[dict[str, Any]]) -> None:
        while True:
            batch = []
            for _ in range(self.BATCH_SIZE):
                try:
                    row: dict[str, Any] = next(iterator)
                except StopIteration:
                    break

                row = self.clean_data(row)

                data = self.model(**row)
                try:
                    data.full_clean()
                except ValidationError as err:
                    sys.stderr.write('Data seems not to be valid.')
                    raise ImproperlyConfiguredFile(err)

                batch.append(data)

            if not batch:
                break

            self.model._default_manager.bulk_create(batch, self.BATCH_SIZE)

    @abc.abstractmethod
    def clean_data(self, row: dict[str, Any]) -> dict[str, Any]:
        ...


class FlightModelMixin:
    def __init__(self, *args: Any, flight_uuid: uuid.UUID | str | None = None, **kwargs: Any) -> None:
        self.flight_uuid = flight_uuid
        self.flight_model = Flight
        self.flight_instance: Flight | None = None
        super().__init__(*args, **kwargs)

    def get_flight_id(self) -> uuid.UUID | str | None:
        return self.flight_uuid


class TelemetryInserter(FlightModelMixin, DataInserter):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        self.height: int | None = None
        super().__init__(*args, **kwargs)

    def clean_data(self, row: dict[str, Any]) -> dict[str, Any]:
        if not self.flight_instance and self.flight_uuid:
            self.flight_instance = self.flight_model.objects.get(id=self.flight_uuid)

        if not self.height:
            self.update_flight_model(row['altitude'])

        row.pop('id')
        row.pop('height')
        row['velocity_x'] = Decimal(str(round(float(row['velocity_x']), 1)))
        row['velocity_y'] = Decimal(str(round(float(row['velocity_y']), 1)))
        row['velocity_z'] = Decimal(str(round(float(row['velocity_z']), 1)))
        row['flight_id'] = self.flight_instance
        return row

    def update_flight_model(self, height: int) -> None:
        self.height = height
        if self.flight_uuid:
            flight = self.flight_model.objects.get(id=self.flight_uuid)
            flight.takeoff_altitude = self.height
            flight.save()


class StatusInserter(FlightModelMixin, DataInserter):
    def clean_data(self, row: dict[str, Any]) -> dict[str, Any]:
        if not self.flight_uuid:
            self.create_flight_model(row['flight_id'], row['time'])

        if not self.flight_instance and self.flight_uuid:
            self.flight_instance = self.flight_model.objects.get(id=self.flight_uuid)

        row.pop('id')
        row['charging'] = row['charging'] == 'TRUE'
        row['flight_id'] = self.flight_instance
        return row

    def create_flight_model(self, uuid: uuid.UUID, time: datetime) -> None:
        self.flight_uuid = uuid
        if self.flight_model.objects.filter(id=self.flight_uuid).exists():
            raise DuplicityFlightError('This flight is already saved in the db.')
        flight = self.flight_model(id=self.flight_uuid, date=time)
        flight.save()
