import os
from typing import Any

from django.conf import settings
from django.core.management.base import BaseCommand, CommandParser

from ...exceptions import ImproperlyConfiguredFile
from ...importers import CSVFileGenerator, StatusSchemaFile, TelemetrySchemaFile
from ...inserters import DataInserter, StatusInserter, TelemetryInserter
from ...models import Status, Telemetry
from ...utils import MapGenerator


class Command(BaseCommand):
    help = "Import new flight data"
    flight_uuid = None

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument('-t', '--telemetry-path', required=True, help='select path for telemetry type file')
        parser.add_argument('-s', '--status-path', required=True, help='select path for status type file')
        return super().add_arguments(parser)

    def handle(self, *args: Any, **kwargs: Any) -> None:
        # verify filename paths
        telemetry_file_path: str = kwargs['telemetry_path']
        status_file_path: str = kwargs['status_path']
        self.validate_path(telemetry_file_path)
        self.validate_path(status_file_path)

        # insert status data
        status_inserter = self.get_inserter(
            StatusInserter,
            status_file_path,
            StatusSchemaFile,
            CSVFileGenerator,
            Status,
        )
        status_inserter.process()

        # insert telemetry data
        flight_uuid = status_inserter.get_flight_id()  # type: ignore[attr-defined]  # TODO:
        telemetry_inserter = self.get_inserter(
            TelemetryInserter,
            telemetry_file_path,
            TelemetrySchemaFile,
            CSVFileGenerator,
            Telemetry,
            flight_uuid=flight_uuid,
        )
        telemetry_inserter.process()

        map_generator = MapGenerator(flight_uuid)
        map_generator.generate_flight_map()

        self.stdout.write('done')

    def validate_path(self, path: str) -> None:
        if not os.path.isfile(path):
            absolute_path = os.path.join(settings.BASE_DIR, path)
            raise ImproperlyConfiguredFile(f'There is no file at {absolute_path}.')

    @staticmethod
    def get_inserter(inserter: type[DataInserter], *args: Any, **kwargs: Any) -> DataInserter:
        return inserter(*args, **kwargs)
