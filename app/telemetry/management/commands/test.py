import subprocess
from typing import Any

from django.core.management.commands.test import Command as BaseCommand


class Command(BaseCommand):
    help = "Custom test suite with mypy."

    def add_arguments(self, parser: Any) -> None:
        parser.add_argument('--mypy', action='store_true', help='Run mypy runner with tests')
        return super().add_arguments(parser)

    def handle(self, *args: Any, **kwargs: Any) -> None:
        if kwargs['mypy']:
            self.stdout.write('Run mypy')
            subprocess.run(['mypy', '.'])
        self.stdout.write('Run test suite')
        super().handle(*args, **kwargs)
        self.stdout.write(self.style.SUCCESS('Done'))
