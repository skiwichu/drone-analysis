from django.urls import path

from .views import FlightDetailView, FlightListView, FlightMapView

urlpatterns = [
    path('', FlightListView.as_view(), name='flight-list'),
    path('<uuid:pk>', FlightDetailView.as_view(), name='flight-detail'),
    path('map/<uuid>', FlightMapView.as_view(), name='flight-map'),
]
