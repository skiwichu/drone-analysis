import uuid
from datetime import datetime, timezone
from decimal import Decimal
from pathlib import Path
from unittest import mock

import haversine
import numpy as np
from django.conf import settings
from django.test import TestCase
from model_bakery import baker

from ..exceptions import SameTimeException, UnsuitableModelException
from ..models import Flight, Status, Telemetry
from ..utils import Correlation, MapGenerator, get_chart_data, get_common_times


class TestMapGenerator(TestCase):
    def setUp(self):
        self.uuid_ = uuid.uuid4()
        self.map_generator = MapGenerator(self.uuid_)

    def test_flight_map_path(self):
        template_dir = Path('/hell/maps')
        file_path = self.map_generator.flight_map_path(template_dir, self.uuid_)
        self.assertEqual(file_path, f'/hell/maps/_flight_map_{self.uuid_}.html')

    @mock.patch('telemetry.utils.MapGenerator.flight_map_path', return_value='/hell/maps/_flight_map_.html')
    @mock.patch('telemetry.utils.folium.PolyLine')
    @mock.patch('telemetry.utils.folium.Map')
    def test_generate_map(self, mock_map, mock_poly_line, _):
        flight = baker.make(Flight, id=self.uuid_)
        coordinates = [
            (50.1, 14.1),
            (50.2, 14.2),
            (50.3, 14.3),
            (50.4, 14.4),
            (50.5, 14.5),
            (50.6, 14.6),
            (50.7, 14.7),
            (50.8, 14.8),
            (50.9, 14.9),
            (51.0, 15.0),
        ]
        for c in coordinates:
            baker.make(Telemetry, flight_id=flight, latitude=c[0], longitude=c[1])
        self.map_generator.generate_flight_map()
        mock_poly_line.assert_called_once()
        mock_map.return_value.save.assert_called_once_with('/hell/maps/_flight_map_.html')

    def test_get_flight_map_path(self):
        template_dir = Path(settings.TEMPLATE_DIRS[0]) / 'maps'
        file_name = template_dir / f"_flight_map_{self.uuid_}.html"
        file_name.touch()
        exist_path = self.map_generator.get_flight_map_path(self.uuid_)
        file_name.unlink()
        nonexist_path = self.map_generator.get_flight_map_path(self.uuid_)
        self.assertEqual(exist_path, file_name.absolute())
        self.assertIsNone(nonexist_path)


class TestGetCommonTimes(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.uuid_ = uuid.uuid4()
        cls.flight = baker.make(Flight, id=cls.uuid_)
        baker.make(Telemetry, time=datetime(2023, 1, 1, 12, 0, 1, tzinfo=timezone.utc), flight_id=cls.flight)
        baker.make(Status, time=datetime(2023, 1, 1, 12, 0, 2, tzinfo=timezone.utc), flight_id=cls.flight)

    def test_get_no_common_times(self):
        times = get_common_times(Telemetry, Status, flight_id=self.uuid_)
        self.assertEqual(times, set())

    def test_get_common_times(self):
        baker.make(Telemetry, time=datetime(2023, 1, 1, 12, 0, 2, tzinfo=timezone.utc), flight_id=self.flight)
        times = get_common_times(Telemetry, Status, flight_id=self.uuid_)
        self.assertEqual(times, {datetime(2023, 1, 1, 12, 0, 2, tzinfo=timezone.utc)})

    def test_raise_invalid_model(self):
        with self.assertRaises(UnsuitableModelException):
            get_common_times(Telemetry, Status, Flight, flight_id=self.uuid_)


class TestComputeHaversine(TestCase):
    @classmethod
    def setUp(cls):
        super().setUpClass()
        cls.corr = Correlation(Telemetry, 'latitude', Telemetry, 'longitude', uuid.uuid4())

    def test_compute_haversine(self):
        coor_1 = (50.1, 14.8)
        coor_2 = (50.2, 14.9)
        time_1 = datetime(2023, 7, 11, 0, 0, 0)
        time_2 = datetime(2023, 7, 11, 0, 0, 2)
        haversine = self.corr.compute_haversine_per_time(coor_1, coor_2, time_1, time_2)
        self.assertAlmostEqual(haversine, -6603.242572642377)

    def test_compute_haversine_in_km(self):
        coor_1 = (50.1, 14.8)
        coor_2 = (50.2, 14.9)
        time_1 = datetime(2023, 7, 11, 0, 0, 0)
        time_2 = datetime(2023, 7, 11, 0, 0, 2)
        haversine_ = self.corr.compute_haversine_per_time(coor_1, coor_2, time_1, time_2, haversine.Unit.KILOMETERS)
        self.assertAlmostEqual(haversine_, -6.603242572642377)

    def test_compute_haversine_no_move(self):
        coor_1 = (50.1, 14.8)
        coor_2 = (50.1, 14.8)
        time_1 = datetime(2023, 7, 11, 0, 0, 0)
        time_2 = datetime(2023, 7, 11, 0, 0, 2)
        haversine = self.corr.compute_haversine_per_time(coor_1, coor_2, time_1, time_2)
        self.assertEqual(haversine, 0)

    def test_compute_haversine_identical_times(self):
        coor_1 = (50.1, 14.8)
        coor_2 = (50.2, 14.9)
        time_1 = datetime(2023, 7, 11, 0, 0, 0)
        time_2 = datetime(2023, 7, 11, 0, 0, 0)
        with self.assertRaises(SameTimeException):
            self.corr.compute_haversine_per_time(coor_1, coor_2, time_1, time_2)


class TestGetModelByValue(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.time = datetime(2023, 1, 1, 12, 0, 2, tzinfo=timezone.utc)
        cls.uuid_ = uuid.uuid4()
        cls.flight = baker.make(Flight, id=cls.uuid_)
        baker.make(Telemetry, flight_id=cls.flight)
        baker.make(Telemetry, flight_id=cls.flight, time=cls.time)
        baker.make(Telemetry)

    def test_get_model_data(self):
        corr = Correlation(Telemetry, 'latitude', Telemetry, 'longitude', self.uuid_)
        data = corr.get_model_data_by_value(Telemetry, 'latitude')
        self.assertEqual(len(data), 2)

    def test_get_model_data_filtered_times(self):
        corr = Correlation(Telemetry, 'latitude', Telemetry, 'longitude', self.uuid_, [self.time])
        data = corr.get_model_data_by_value(Telemetry, 'latitude')
        self.assertEqual(len(data), 1)


class TestGetModelByTimeReceived(TestCase):
    def setUp(self):
        self.time = datetime(2023, 1, 1, 12, 0, 1, tzinfo=timezone.utc)
        time_received = datetime(2023, 1, 1, 12, 0, 2, 500000, tzinfo=timezone.utc)
        self.uuid_ = uuid.uuid4()
        flight = baker.make(Flight, id=self.uuid_)
        baker.make(Telemetry, flight_id=flight, time=self.time, time_received=time_received)
        baker.make(Telemetry)
        baker.make(Status)

    def test_get_model_data(self):
        corr = Correlation(Telemetry, 'time_received', Status, 'time', self.uuid_, [self.time])
        data = corr.get_model_data_by_time_received(Telemetry, 'time_received')
        self.assertEqual(data[0], 1500000)

    def test_wrong_model_no_data(self):
        corr = Correlation(Telemetry, 'time_received', Status, 'time', self.uuid_, [self.time])
        data = corr.get_model_data_by_time_received(Status, 'time')
        self.assertIsNone(data)

    def test_get_model_no_filtered_times(self):
        corr = Correlation(Telemetry, 'time_received', Status, 'time', self.uuid_)
        data = corr.get_model_data_by_time_received(Telemetry, 'time_received')
        self.assertIsNone(data)


class TestConvertDataToNumpy(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        baker.make(Telemetry, _quantity=3)
        cls.corr = Correlation(Telemetry, 'latitude', Telemetry, 'longitude', uuid.uuid4())

    def test_convert_queryset_to_numpy(self):
        queryset = Telemetry.objects.values_list('latitude', flat=True)
        data = self.corr.convert_data_to_numpy(queryset)
        self.assertEqual(type(data[0]), np.float64)
        self.assertEqual(data[0], queryset[0])

    def test_convert_sequence_to_numpy(self):
        sequence = [Decimal('1.0'), Decimal('2.0'), Decimal('3.0')]
        data = self.corr.convert_data_to_numpy(sequence)
        self.assertEqual(type(data[0]), np.float64)
        self.assertEqual(data[0], sequence[0])

    def test_convert_to_numpy_datetime(self):
        sequence = [datetime(2023, 1, 1, 12, 0, 0, 500) for _ in range(3)]
        data = self.corr.convert_data_to_numpy(sequence)
        self.assertEqual(type(data[0]), np.float64)
        self.assertEqual(data[0], 1672570800.0005)


class TestGetHaversine(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        uuid_ = uuid.uuid4()
        flight = baker.make(Flight, id=uuid_)
        coordinates = [(50.1, 14.1), (50.2, 14.2), (50.3, 14.3), (50.4, 14.4)]
        times = [datetime(2023, 1, 1, 12, 0, i, tzinfo=timezone.utc) for i in range(1, 5)]

        for z in zip(coordinates, times):
            baker.make(Telemetry, flight_id=flight, latitude=z[0][0], longitude=z[0][1], time=z[1])
        cls.corr = Correlation(Telemetry, 'latitude longitude', Telemetry, 'time', uuid_)

    def test_get_haversine(self):
        data = self.corr.get_haversine(Telemetry, 'latitude longitude')
        self.assertEqual(len(data), 3)
        self.assertAlmostEqual(data[0], -13206.485145284754)

    def test_get_no_haversine(self):
        data = self.corr.get_haversine(Telemetry, 'time')
        self.assertIsNone(data)


class TestGetVelocity(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        uuid_ = uuid.uuid4()
        flight = baker.make(Flight, id=uuid_)
        velocities = [(3.0, 4.0), (-7.4, -3.2), (-7.5, -3.0)]
        for vel in velocities:
            baker.make(Telemetry, flight_id=flight, velocity_x=vel[0], velocity_y=vel[1])
        cls.corr = Correlation(Telemetry, 'velocity_x velocity_y', Telemetry, 'time', uuid_)

    def test_velocity(self):
        data = self.corr.get_velocity(Telemetry, 'velocity_x velocity_y')
        self.assertEqual(len(data), 3)
        self.assertAlmostEqual(data[0], 5.0)

    def test_get_no_haversine(self):
        data = self.corr.get_velocity(Telemetry, 'time')
        self.assertIsNone(data)


class TestCorrelation(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        uuid_ = uuid.uuid4()
        flight = baker.make(Flight, id=uuid_)
        velocity_x = [1, 2, 3, 4, 5]
        velocity_y = [1, 2, 3, 4, 5]
        for vel in zip(velocity_x, velocity_y):
            baker.make(Telemetry, flight_id=flight, velocity_x=vel[0], velocity_y=vel[1])
        cls.corr = Correlation(Telemetry, 'velocity_x', Telemetry, 'velocity_y', uuid_)

    def test_correlation(self):
        corr = self.corr.run()
        self.assertAlmostEqual(corr, 1.0)


class TestChartData(TestCase):
    def setUp(self):
        self.uuid_ = uuid.uuid4()
        flight = baker.make(Flight, id=self.uuid_)
        baker.make(
            Status,
            flight_id=flight,
            time=datetime(2023, 1, 1, 12, 0, 0, tzinfo=timezone.utc),
            rsrp=0,
            rsrq=0,
            snr=0,
        )
        baker.make(
            Telemetry,
            flight_id=flight,
            time=datetime(2023, 1, 1, 12, 0, 0, tzinfo=timezone.utc),
            time_received=datetime(2023, 1, 1, 12, 0, 0, 500000, tzinfo=timezone.utc),
        )

    def test_get_chart_data(self):
        data = get_chart_data(self.uuid_)
        self.assertEqual(data[0]['time'], datetime(2023, 1, 1, 12, 0, 0, tzinfo=timezone.utc))
        self.assertAlmostEqual(data[0]['time_delay'], 0.5)
        self.assertEqual(data[0]['rsrp'], 0)
        self.assertEqual(data[0]['rsrq'], 0)
        self.assertEqual(data[0]['snr'], 0)
