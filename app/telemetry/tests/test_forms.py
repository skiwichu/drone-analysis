from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from ..forms import UploadFileForm


class TestUplaodFileForm(TestCase):
    def test_valid_upload_file(self):
        telemetry_file = SimpleUploadedFile("telemetry.csv", b"telemetry_content", content_type='text/csv')
        status_file = SimpleUploadedFile('status.csv', b"status_content", content_type='text/csv')
        form = UploadFileForm(None, {'telemetry_file': telemetry_file, 'status_file': status_file})
        self.assertTrue(form.is_valid())

    def test_invalid_upload_file(self):
        telemetry_file = SimpleUploadedFile("telemetry.csv", b"telemetry_content")
        no_file = 'abcde'
        form = UploadFileForm(None, {'telemetry_file': telemetry_file, 'status_file': no_file})
        self.assertFalse(form.is_valid())
