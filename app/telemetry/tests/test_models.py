import uuid
from datetime import UTC, datetime
from decimal import Decimal

from django.core.exceptions import ValidationError
from django.test import TestCase
from model_bakery import baker

from ..models import Flight, Status, Telemetry


class FlightTest(TestCase):
    def setUp(self):
        self.uuid_ = 'a005c583-3df0-4193-b6af-8dfb2d4038c2'
        self.date = datetime(2023, 12, 24, 19, 0, 1, tzinfo=UTC)
        self.takeoff_altitude = Decimal('100.12')
        self.flight = Flight.objects.create(
            id=self.uuid_,
            date=self.date,
            takeoff_altitude=self.takeoff_altitude,
        )
        try:
            self.flight.full_clean()
        except ValidationError as err:
            self.fail(err)

    def test_valid_fields(self):
        flight = Flight.objects.first()
        self.assertEqual(flight.id, uuid.UUID(self.uuid_))
        self.assertEqual(flight.date, self.date)
        self.assertTrue(flight.created_at)
        self.assertEqual(flight.takeoff_altitude, self.takeoff_altitude)

    def test_str(self):
        self.assertEqual(
            str(self.flight),
            "2023-12-24 19:00: a005c583-3df0-4193-b6af-8dfb2d4038c2",
        )

    def test_takeoff_altitude(self):
        # default value
        f = Flight(id=uuid.uuid4(), date=datetime(2023, 12, 24, 19, 0, 1, tzinfo=UTC))
        f.save()
        self.assertEqual(Flight.objects.count(), 2)
        self.assertIsNone(f.takeoff_altitude)

    def test_valid_altitude(self):
        f = baker.make(Flight, takeoff_altitude=-500)
        with self.assertRaises(ValidationError):
            f.full_clean()

        f = baker.make(Flight, takeoff_altitude=20000)
        with self.assertRaises(ValidationError):
            f.full_clean()

    def test_get_absolute_url(self):
        url = self.flight.get_absolute_url()
        self.assertEqual(url, f'/{self.uuid_}')


class TelemetryTest(TestCase):
    def setUp(self):
        self.time = datetime(2023, 12, 24, 19, 0, 1, tzinfo=UTC)
        self.time_received = datetime(2023, 12, 24, 19, 0, 1, 123, tzinfo=UTC)
        self.latitude = 0.12345
        self.longitude = -50.12345
        self.altitude = Decimal('1000.12')
        self.geo_altitude = Decimal('1000.5')
        self.velocity_x = 1
        self.velocity_y = 1
        self.velocity_z = 1
        self.horizontal_accuracy = 1
        self.vertical_accuracy = 1
        self.speed_accuracy = 1
        self.pressure = 1023.456
        self.flight = baker.make(Flight)
        self.telemetry = Telemetry.objects.create(
            time=self.time,
            time_received=self.time_received,
            latitude=self.latitude,
            longitude=self.longitude,
            altitude=self.altitude,
            geo_altitude=self.geo_altitude,
            velocity_x=self.velocity_x,
            velocity_y=self.velocity_y,
            velocity_z=self.velocity_z,
            horizontal_accuracy=self.horizontal_accuracy,
            vertical_accuracy=self.vertical_accuracy,
            speed_accuracy=self.speed_accuracy,
            pressure=self.pressure,
            flight_id=self.flight,
        )
        try:
            self.telemetry.full_clean()
        except ValidationError as err:
            self.fail(err)

    def test_valid_fields(self):
        telemetry = Telemetry.objects.first()
        self.assertEqual(telemetry.time, self.time)
        self.assertEqual(telemetry.time_received, self.time_received)
        self.assertEqual(telemetry.latitude, self.latitude)
        self.assertEqual(telemetry.longitude, self.longitude)
        self.assertEqual(telemetry.altitude, self.altitude)
        self.assertEqual(telemetry.geo_altitude, self.geo_altitude)
        self.assertEqual(telemetry.velocity_x, self.velocity_x)
        self.assertEqual(telemetry.velocity_y, self.velocity_y)
        self.assertEqual(telemetry.velocity_z, self.velocity_z)
        self.assertEqual(telemetry.horizontal_accuracy, self.horizontal_accuracy)
        self.assertEqual(telemetry.vertical_accuracy, self.vertical_accuracy)
        self.assertEqual(telemetry.speed_accuracy, self.speed_accuracy)
        self.assertEqual(telemetry.pressure, self.pressure)
        self.assertEqual(telemetry.flight_id, self.flight)

    def test_str(self):
        telemetry = Telemetry.objects.first()
        time = self.time.strftime('%Y-%m-%d %H:%M')
        self.assertEqual(str(telemetry), f"{str(self.flight.id)}: {time}")

    def test_latitude(self):
        # validators
        t = baker.make(Telemetry, latitude=-100)
        with self.assertRaises(ValidationError):
            t.full_clean()

        t = baker.make(Telemetry, latitude=100)
        with self.assertRaises(ValidationError):
            t.full_clean()

    def test_longitude(self):
        # validators
        t = baker.make(Telemetry, longitude=-200)
        with self.assertRaises(ValidationError):
            t.full_clean()

        t = baker.make(Telemetry, longitude=200)
        with self.assertRaises(ValidationError):
            t.full_clean()

    def test_altitude(self):
        # validators
        t = baker.make(Telemetry, altitude=-500)
        with self.assertRaises(ValidationError):
            t.full_clean()

        t = baker.make(Telemetry, altitude=20000)
        with self.assertRaises(ValidationError):
            t.full_clean()

    def test_get_altitude(self):
        # validators
        t = baker.make(Telemetry, geo_altitude=-500)
        with self.assertRaises(ValidationError):
            t.full_clean()

        t = baker.make(Telemetry, geo_altitude=20000)
        with self.assertRaises(ValidationError):
            t.full_clean()

    def test_pressure(self):
        # validators
        t = baker.make(Telemetry, pressure=700)
        with self.assertRaises(ValidationError):
            t.full_clean()

        t = baker.make(Telemetry, pressure=1200)
        with self.assertRaises(ValidationError):
            t.full_clean()

    def test_flight_id(self):
        Flight.objects.first().delete()
        self.assertEqual(Telemetry.objects.count(), 0)


class StatusTest(TestCase):
    def setUp(self):
        self.time = datetime(2023, 12, 24, 19, 0, 1, tzinfo=UTC)
        self.time_received = datetime(2023, 12, 24, 19, 0, 1, 123, tzinfo=UTC)
        self.battery = 4.0123456789
        self.charging = False
        self.satellites = 11
        self.cellid = '219A0D00'
        self.tac = 'BCAC'
        self.rsrp = -85
        self.rsrq = -9
        self.snr = 9
        self.flight = baker.make(Flight)
        self.status = Status.objects.create(
            time=self.time,
            time_received=self.time_received,
            battery=self.battery,
            charging=self.charging,
            satellites=self.satellites,
            cellid=self.cellid,
            tac=self.tac,
            rsrp=self.rsrp,
            rsrq=self.rsrq,
            snr=self.snr,
            flight_id=self.flight,
        )
        try:
            self.status.full_clean()
        except ValidationError as err:
            self.fail(err)

    def test_valid_fields(self):
        status = Status.objects.first()
        self.assertEqual(status.time, self.time)
        self.assertEqual(status.time_received, self.time_received)
        self.assertEqual(status.battery, self.battery)
        self.assertEqual(status.charging, self.charging)
        self.assertEqual(status.satellites, self.satellites)
        self.assertEqual(status.cellid, self.cellid)
        self.assertEqual(status.tac, self.tac)
        self.assertEqual(status.rsrp, self.rsrp)
        self.assertEqual(status.rsrq, self.rsrq)
        self.assertEqual(status.snr, self.snr)
        self.assertEqual(status.flight_id, self.flight)

    def test_str(self):
        status = Status.objects.first()
        time = self.time.strftime('%Y-%m-%d %H:%M')
        self.assertEqual(str(status), f"{str(self.flight.id)}: {time}")

    def test_cellid(self):
        # validators
        s = baker.make(Status, cellid='ABCDEFG')
        with self.assertRaises(ValidationError):
            s.full_clean()

        s = baker.make(Status, cellid='ABCDEFGHI')
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test_tac(self):
        s = baker.make(Status, cellid='ABC')
        with self.assertRaises(ValidationError):
            s.full_clean()

        s = baker.make(Status, cellid='ABCDE')
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test_flight_id(self):
        Flight.objects.first().delete()
        self.assertEqual(Status.objects.count(), 0)
