import http
import os
import uuid
import warnings
from datetime import datetime, timezone
from pathlib import Path
from unittest import mock

from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, override_settings
from django.urls import reverse
from model_bakery import baker

from ..models import Flight, Status, Telemetry
from . import test_storages

warnings.filterwarnings("ignore")


@override_settings(STORAGES=test_storages)
class TestFlightMapView(TestCase):
    def setUp(self):
        self.uuid_ = uuid.uuid4()
        self.template_dir = Path(settings.TEMPLATE_DIRS[0]) / 'maps'
        self.file_name = self.template_dir / f"_flight_map_{self.uuid_}.html"
        self.file_name.touch()

    def test_urls(self):
        url = reverse('flight-map', args=[self.uuid_])
        self.assertEqual(url, f'/map/{self.uuid_}')

    def test_valid_response(self):
        response = self.client.get(reverse('flight-map', args=[self.uuid_]))
        self.assertEqual(response.status_code, http.HTTPStatus.OK)

        expected_path = os.path.join(self.template_dir, f'_flight_map_{self.uuid_}.html')
        self.assertTemplateUsed(response, expected_path)

    def test_invalid_responses(self):
        uuid_ = uuid.uuid4()
        response = self.client.get(reverse('flight-map', args=[uuid_]))
        self.assertEqual(response.status_code, http.HTTPStatus.NOT_FOUND)

        non_uuid = 'kocicka'
        response = self.client.get(reverse('flight-map', args=[non_uuid]))
        self.assertEqual(response.status_code, http.HTTPStatus.NOT_FOUND)

    def tearDown(self):
        self.file_name.unlink()


@override_settings(STORAGES=test_storages)
class TestFlightListView(TestCase):
    def setUp(self):
        baker.make(Flight, _quantity=2)

    def test_urls(self):
        url = reverse('flight-list')
        self.assertEqual(url, '/')

    def test_response(self):
        response = self.client.get(reverse('flight-list'))
        header = Flight._meta.fields
        self.assertEqual(response.status_code, http.HTTPStatus.OK)
        self.assertEqual(response.context['header'], header)
        self.assertEqual(len(response.context['object_list']), 2)

    @mock.patch('telemetry.views.call_command')
    def test_form(self, mock_command):
        telemetry_file = SimpleUploadedFile("telemetry.csv", b"telemetry_content", content_type='text/csv')
        status_file = SimpleUploadedFile('status.csv', b"status_content", content_type='text/csv')
        response = self.client.post(
            reverse('flight-list'),
            {'telemetry_file': telemetry_file, 'status_file': status_file},
        )
        mock_command.assert_called_once()
        self.assertEqual(response.status_code, http.HTTPStatus.FOUND)


@override_settings(STORAGES=test_storages)
class TestFlightDetailView(TestCase):
    def setUp(self):
        self.uuid_ = uuid.uuid4()
        self.flight = baker.make(Flight, id=self.uuid_)
        for i in range(3):
            baker.make(
                Telemetry,
                flight_id=self.flight,
                time=datetime(2023, 1, 1, 12, 0, i, tzinfo=timezone.utc),
                time_received=datetime(2023, 1, 1, 12, 0, i, 500000, tzinfo=timezone.utc),
                latitude=50,
                longitude=50,
            )
            baker.make(Status, flight_id=self.flight, time=datetime(2023, 1, 1, 12, 0, i, tzinfo=timezone.utc))

    def test_urls(self):
        url = reverse('flight-detail', args=[self.uuid_])
        self.assertEqual(url, f'/{self.uuid_}')

    def test_invalid_responses(self):
        uuid_ = uuid.uuid4()
        response = self.client.get(f'/{uuid_}')
        self.assertEqual(response.status_code, http.HTTPStatus.NOT_FOUND)

        response = self.client.get('/pejsek')
        self.assertEqual(response.status_code, http.HTTPStatus.NOT_FOUND)

    def test_response(self):
        response = self.client.get(reverse('flight-detail', args=[self.uuid_]))
        self.assertEqual(response.status_code, http.HTTPStatus.OK)
        self.assertIsNotNone(response.context['corr_altitude_geoaltitude'])
        self.assertIsNotNone(response.context['corr_velocity_coordinates'])
        self.assertIsNotNone(response.context['corr_time_received_rsrp'])
        self.assertIsNotNone(response.context['corr_time_received_rsrq'])
        self.assertIsNotNone(response.context['corr_time_received_snr'])
        self.assertEqual(len(response.context['chart_labels']), 3)
        self.assertEqual(len(response.context['chart_time_delay']), 3)
        self.assertEqual(len(response.context['chart_rsrp']), 3)
        self.assertEqual(len(response.context['chart_rsrq']), 3)
        self.assertEqual(len(response.context['chart_snr']), 3)
