import uuid
from datetime import datetime, timezone

from django.db.models import F
from django.test import TestCase
from model_bakery import baker

from ..models import Flight, Telemetry
from ..models.functions import Epoch, EpochWithMilliseconds, MilliToMicro


class TestEpoch(TestCase):
    def test_epoch(self):
        date = datetime(2023, 1, 1, 12, 0, 0, tzinfo=timezone.utc)
        baker.make(Telemetry, time=date)
        epoch = Telemetry.objects.annotate(epoch_time=Epoch(F('time'))).first().epoch_time
        self.assertEqual(epoch, datetime.timestamp(date))


class TestEpochWithMilliseconds(TestCase):
    def test_epoch(self):
        date = datetime(2023, 1, 1, 12, 0, 0, 5000, tzinfo=timezone.utc)
        baker.make(Telemetry, time_received=date)
        epoch = Telemetry.objects.annotate(epoch_time=EpochWithMilliseconds(F('time_received'))).first().epoch_time
        self.assertEqual(epoch, datetime.timestamp(date))

    def test_sophisticatd_epoch(self):
        date = datetime(2023, 1, 1, 12, 0, 0, 5000, tzinfo=timezone.utc)
        uuid_ = uuid.uuid4()
        flight = baker.make(Flight, id=uuid_)
        baker.make(Telemetry, flight_id=flight, time_received=date)
        epoch = (
            Telemetry.objects.filter(flight_id=flight)
            .annotate(epoch_time=EpochWithMilliseconds(F('time_received')))
            .first()
            .epoch_time
        )
        self.assertEqual(epoch, datetime.timestamp(date))


class TestMilliToMicro(TestCase):
    def test_milli_to_micro(self):
        baker.make(Telemetry, geo_altitude=1)
        micro = Telemetry.objects.annotate(micro=MilliToMicro(F('geo_altitude'))).first().micro
        self.assertEqual(micro, 1 * 1_000_000)
