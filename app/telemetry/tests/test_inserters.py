import uuid
import warnings
from datetime import datetime
from unittest import mock

from django.test import TestCase
from model_bakery import baker

from ..exceptions import DuplicityFlightError
from ..importers import CSVFileGenerator, StatusSchemaFile, TelemetrySchemaFile
from ..inserters import DataInserter, FlightModelMixin, StatusInserter, TelemetryInserter
from ..models import Flight, Status, Telemetry
from . import fake_status_generator, fake_telemetry_generator

warnings.filterwarnings("ignore")


class FakeDataInserter(DataInserter):
    def clean_data(self, row):
        return row


class TestDataInserter(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.path = '/test/hell'
        cls.schema = StatusSchemaFile
        cls.file_generator = CSVFileGenerator
        cls.mock_model = mock.MagicMock()
        cls.mock_model.objects.bulk_create = mock.MagicMock()
        cls.data_inserter = FakeDataInserter(cls.path, cls.schema, cls.file_generator, cls.mock_model)

    def test_init(self):
        self.assertEqual(self.data_inserter.path, self.path)
        self.assertEqual(self.data_inserter.schema, self.schema)
        self.assertEqual(self.data_inserter.file_generator.path, self.path)
        self.assertEqual(self.data_inserter.file_generator.schema, self.schema)
        self.assertEqual(self.data_inserter.model, self.mock_model)

    def test_inserting_data(self):
        self.data_inserter.insert_data(fake_status_generator())
        self.assertEqual(self.mock_model.call_count, 3)
        self.mock_model._default_manager.bulk_create.assert_called_once()


class TestFlightModelMixin(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.path = '/test/hell'
        cls.schema = StatusSchemaFile
        cls.file_generator = CSVFileGenerator
        cls.model = Status
        cls.uuid_ = uuid.uuid4()

        class FakeDataInserterWithMixin(FlightModelMixin, FakeDataInserter):
            pass

        cls.data_inserter = FakeDataInserterWithMixin(
            cls.path,
            cls.schema,
            cls.file_generator,
            cls.model,
            flight_uuid=cls.uuid_,
        )

    def test_init(self):
        self.assertEqual(self.data_inserter.path, self.path)
        self.assertEqual(self.data_inserter.schema, self.schema)
        self.assertEqual(self.data_inserter.file_generator.path, self.path)
        self.assertEqual(self.data_inserter.file_generator.schema, self.schema)
        self.assertEqual(self.data_inserter.model, self.model)
        self.assertEqual(self.data_inserter.flight_uuid, self.uuid_)
        self.assertEqual(self.data_inserter.flight_model, Flight)
        self.assertIsNone(self.data_inserter.flight_instance)

    def test_get_flight_id(self):
        uuid_ = self.data_inserter.get_flight_id()
        self.assertEqual(uuid_, self.uuid_)


class TestTelemetryInserter(TestCase):
    def setUp(self):
        self.path = '/test/hell'
        self.schema = TelemetrySchemaFile
        self.file_generator = CSVFileGenerator
        self.model = Telemetry
        self.uuid_ = uuid.uuid4()
        self.flight = baker.make(Flight, id=self.uuid_)
        self.data_inserter = TelemetryInserter(
            self.path,
            self.schema,
            self.file_generator,
            self.model,
            flight_uuid=self.uuid_,
        )

    def test_init(self):
        self.assertIsNone(self.data_inserter.height)

    def test_insert_data(self):
        self.data_inserter.insert_data(fake_telemetry_generator())
        tele = Telemetry.objects.count()
        self.assertEqual(tele, 3)

    def test_update_flight_model(self):
        self.data_inserter.update_flight_model(5.0)
        self.flight.refresh_from_db()
        self.assertEqual(self.flight.takeoff_altitude, 5.0)


class TestStatusInserter(TestCase):
    def setUp(self):
        self.path = '/test/hell'
        self.schema = StatusSchemaFile
        self.file_generator = CSVFileGenerator
        self.model = Status
        self.uuid_ = uuid.uuid4()
        # self.flight = baker.make(Flight, id=self.uuid_)
        self.data_inserter = StatusInserter(self.path, self.schema, self.file_generator, self.model)

    def test_insert_data(self):
        self.data_inserter.insert_data(fake_status_generator())
        statuses = Status.objects.count()
        self.assertEqual(statuses, 3)

    def test_create_flight_model(self):
        self.data_inserter.create_flight_model(self.uuid_, time=datetime.now())
        self.assertEqual(Flight.objects.count(), 1)
        with self.assertRaises(DuplicityFlightError):
            self.data_inserter.create_flight_model(self.uuid_, time=datetime.now())
