import pathlib
import tempfile
from unittest import mock

from django.core.management import call_command
from django.test import TestCase

from ..exceptions import ImproperlyConfiguredFile
from ..management.commands.import_new_flight import Command, MapGenerator, StatusInserter, TelemetryInserter


class TestCommand(TestCase):
    def test_validate_path(self):
        with tempfile.TemporaryDirectory() as dir_name:
            dir = pathlib.Path(dir_name)
            tele_file = dir / 'tele_file.txt'
            tele_file.touch()
            no_file = 'no_file.txt'
            command = Command()
            command.validate_path(tele_file)
            with self.assertRaises(ImproperlyConfiguredFile):
                command.validate_path(no_file)

    @mock.patch.object(MapGenerator, 'generate_flight_map')
    @mock.patch.object(TelemetryInserter, 'process')
    @mock.patch.object(StatusInserter, 'process')
    def test_handler(self, mock_status_inserter, mock_telemetry_inserter, mock_map_generator):
        with tempfile.TemporaryDirectory() as dir_name:
            dir = pathlib.Path(dir_name)
            tele_file = dir / 'tele_file.txt'
            tele_file.touch()
            status_file = dir / 'tele_file.txt'
            status_file.touch()
            call_command("import_new_flight", '-t', f"{tele_file}", '-s', f"{status_file}")
            mock_status_inserter.assert_called_once()
            mock_telemetry_inserter.assert_called_once()
            mock_map_generator.assert_called_once()
