import os
from typing import NamedTuple
from unittest import mock

from django.test import TestCase

from ..exceptions import ImproperlyConfiguredFile
from ..importers import CSVFileGenerator, InputFileGenerator

fake_csv_file = """name,id,expletive
Jan,1,friend
Milos,2,brick
"""
fake_csv_file_2 = """name,id,expletive
Mirek,3,kitten
"""
fake_csv_file_3 = """expletive,expletive,expletive
puppy,poopy,chocolate
"""
fake_csv_file_4 = os.urandom(50)


class TestFileGenerator(InputFileGenerator):
    def iterate_file(self, path=None, schema=None):
        pass


class TestInputFileGenerator(TestCase):
    def test_input_file_generator(self):
        EmployeeSchema = NamedTuple('EmployeeSchema', name=str, id=int, expletive=str)  # noqa: N806
        fake_generator = TestFileGenerator('/test/hell/', EmployeeSchema)
        self.assertEqual(fake_generator.path, '/test/hell/')
        self.assertEqual(fake_generator.schema, EmployeeSchema)


class TestCSVFileGenerator(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.fake_csv_file = fake_csv_file
        cls.file_path = '/test/hell/'
        cls.EmployeeSchema = NamedTuple('EmployeeSchema', name=str, id=int, expletive=str)  # noqa: N806
        cls.csv_generator = CSVFileGenerator(cls.file_path, cls.EmployeeSchema)

    @mock.patch.object(CSVFileGenerator, 'validate_csv_format', return_value=0)
    def test_iterate_file(self, _):
        with mock.patch('telemetry.importers.open', mock.mock_open(read_data=self.fake_csv_file)) as mock_file:
            generator = self.csv_generator.iterate_file()
            self.assertEqual(next(generator), {'name': 'Jan', 'id': '1', 'expletive': 'friend'})
            self.assertEqual(next(generator), {'name': 'Milos', 'id': '2', 'expletive': 'brick'})
            with self.assertRaises(StopIteration):
                next(generator)
            mock_file.assert_called_once_with(self.file_path)

    @mock.patch.object(CSVFileGenerator, 'validate_csv_format', return_value=0)
    def test_iterate_file_with_variables(self, _):
        with mock.patch('telemetry.importers.open', mock.mock_open(read_data=fake_csv_file_2)) as mock_file:
            generator = self.csv_generator.iterate_file('/test/test/', self.EmployeeSchema)
            self.assertEqual(next(generator), {'name': 'Mirek', 'id': '3', 'expletive': 'kitten'})
            with self.assertRaises(StopIteration):
                next(generator)
            mock_file.assert_called_once_with('/test/test/')

    @mock.patch.object(CSVFileGenerator, 'validate_csv_format', return_value=0)
    def test_invalid_csv_file(self, _):
        with mock.patch('telemetry.importers.open', mock.mock_open(read_data=fake_csv_file_3)) as mock_file:
            generator = self.csv_generator.iterate_file()
            with self.assertRaises(ImproperlyConfiguredFile):
                next(generator)
            mock_file.assert_called_once_with(self.file_path)

    def test_invalid_csv_format(self):
        with mock.patch('telemetry.importers.open', mock.mock_open(read_data=fake_csv_file_4)) as mock_file:
            with self.assertRaises(ImproperlyConfiguredFile):
                generator = self.csv_generator.iterate_file()
                next(generator)
            mock_file.assert_called_once_with(self.file_path)
