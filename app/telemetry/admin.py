from django.contrib import admin

from .models import Flight, Status, Telemetry

admin.site.register(Flight)
admin.site.register(Status)
admin.site.register(Telemetry)
