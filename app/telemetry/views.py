import uuid
from typing import Any

from django.contrib import messages
from django.core.management import call_command
from django.db.models import QuerySet
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, TemplateView
from django.views.generic.edit import FormView

from .exceptions import DuplicityFlightError, ImproperlyConfiguredFile
from .forms import UploadFileForm
from .models import Flight, Status, Telemetry
from .utils import Correlation, MapGenerator, get_chart_data, get_common_times


class FlightListView(FormView[UploadFileForm], ListView[Flight]):
    template_name = 'flight_list_view.html'
    model = Flight
    form_class = UploadFileForm
    success_url = reverse_lazy('flight-list')

    def get_queryset(self) -> QuerySet[Flight]:
        return self.model.objects.order_by('-date')

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['header'] = self.model._meta.fields
        return context

    def form_valid(self, form: UploadFileForm) -> HttpResponse:
        telemetry_file_path = self.request.FILES.get('telemetry_file').temporary_file_path()  # type: ignore[union-attr]  # noqa: E501
        status_file_path = self.request.FILES.get('status_file').temporary_file_path()  # type: ignore[union-attr]
        try:
            call_command("import_new_flight", '-t', f"{telemetry_file_path}", '-s', f"{status_file_path}")
        except (DuplicityFlightError, ImproperlyConfiguredFile) as err:
            messages.add_message(self.request, messages.WARNING, str(err))
        return HttpResponseRedirect(self.get_success_url())


class FlightMapView(TemplateView):
    def get_template_names(self) -> list[str]:
        flight_id = self.kwargs.get('uuid')
        try:
            uuid_ = uuid.UUID(flight_id)
        except ValueError:
            raise Http404
        template_name = MapGenerator.get_flight_map_path(uuid_)
        if template_name:
            return [str(template_name)]
        else:
            raise Http404


class FlightDetailView(TemplateView):
    template_name = 'flight_detail_view.html'

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        flight_id: uuid.UUID = self.get_flight_id()
        get_object_or_404(Flight, id=flight_id)
        context['flight_id'] = flight_id

        # correlations
        corr_altitude_geoaltitude = Correlation(Telemetry, 'altitude', Telemetry, 'geo_altitude', flight_id)
        context['corr_altitude_geoaltitude'] = round(corr_altitude_geoaltitude.run(), 2)
        corr_velocity_coordinates = Correlation(
            Telemetry,
            'velocity_x velocity_y',
            Telemetry,
            'latitude longitude',
            flight_id,
        )
        context['corr_velocity_coordinates'] = round(corr_velocity_coordinates.run(), 2)
        filtered_times = get_common_times(Status, Telemetry, flight_id=flight_id)
        corr_time_received_rsrp = Correlation(Telemetry, 'time_received', Status, 'rsrp', flight_id, filtered_times)
        context['corr_time_received_rsrp'] = round(corr_time_received_rsrp.run(), 2)
        corr_time_received_rsrq = Correlation(Telemetry, 'time_received', Status, 'rsrq', flight_id, filtered_times)
        context['corr_time_received_rsrq'] = round(corr_time_received_rsrq.run(), 2)
        corr_time_received_snr = Correlation(Telemetry, 'time_received', Status, 'snr', flight_id, filtered_times)
        context['corr_time_received_snr'] = round(corr_time_received_snr.run(), 2)

        # chart data
        chart_data = get_chart_data(flight_id)
        context['chart_labels'] = [i['time'].strftime("%H:%M:%S") for i in chart_data]
        context['chart_time_delay'] = [str(i['time_delay']) for i in chart_data]
        context['chart_rsrp'] = [i['rsrp'] for i in chart_data]
        context['chart_rsrq'] = [i['rsrq'] for i in chart_data]
        context['chart_snr'] = [i['snr'] for i in chart_data]
        return context

    def get_flight_id(self) -> uuid.UUID:
        return self.kwargs.get('pk')
