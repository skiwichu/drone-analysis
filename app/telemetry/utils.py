import math
import os
import uuid
from collections.abc import Iterable, Sequence
from datetime import datetime
from decimal import Decimal
from pathlib import Path
from typing import Any, Protocol, TypeVar

import folium
import haversine
import numpy as np
import numpy.typing as npt
from django.conf import settings
from django.db.models import F, Model, QuerySet

from .exceptions import DifferentLengthException, ImproperlyConfiguredFile, SameTimeException, UnsuitableModelException
from .models import Epoch, EpochWithMilliseconds, MilliToMicro, Status, Telemetry

Number_type = TypeVar("Number_type", float, int, Decimal)


class GetValue(Protocol):
    def __call__(self, model: Telemetry | Status, value: str) -> list[float] | None:
        ...


class MapGenerator:
    def __init__(self, flight_id: str | uuid.UUID) -> None:
        self.flight_id = flight_id
        self.template_dir: Path = settings.TEMPLATE_DIRS[0] / 'maps'  # type: ignore[index]

    def generate_flight_map(self) -> None:
        """Creates a html map of the route of the flight."""
        coordinates = Telemetry.objects.filter(flight_id=self.flight_id).values('latitude', 'longitude')

        avg_lat = sum(c['latitude'] for c in coordinates) / len(coordinates)
        avg_lon = sum(c['longitude'] for c in coordinates) / len(coordinates)

        route_map = folium.Map(location=[avg_lat, avg_lon], zoom_start=13)
        folium.PolyLine(
            [(i['latitude'], i['longitude']) for i in coordinates], color="red", weight=2.5, opacity=1
        ).add_to(route_map)
        file_path = self.flight_map_path(self.template_dir, self.flight_id)
        route_map.save(file_path)

    @staticmethod
    def flight_map_path(template_dir: Path, flight_id: str | uuid.UUID) -> str:
        """Helper to the function 'generate_flight_map' to set a path for a html map,"""
        return os.path.join(template_dir, settings.FLIGHT_MAP_PATH.format(flight_id))

    @staticmethod
    def get_flight_map_path(flight_id: uuid.UUID) -> str | None:
        uuid_ = str(flight_id)
        template_path = settings.TEMPLATE_DIRS[0] / 'maps'  # type: ignore[index]
        for i in template_path.iterdir():
            if uuid_ in str(i):
                return i
        return None


def get_common_times(*models: type[Model], flight_id: str | uuid.UUID) -> set[datetime]:
    """
    Various model have can have different timestamp.
    The function finds the set of common times in selected models.
    """
    # check if models are appropriate
    for model in models:
        fields = [i.name for i in model._meta.fields]
        if not ('flight_id' or 'time') in fields:
            raise UnsuitableModelException(f'Model {model._meta.verbose_name} does not have expected fields.')

    intersection = set(models[0]._default_manager.filter(flight_id=flight_id).values_list('time', flat=True))
    for m in models[1:]:
        intersection &= set(m._default_manager.filter(flight_id=flight_id).values_list('time', flat=True))
    return intersection


class Correlation:
    def __init__(
        self,
        model_x: type[Telemetry] | type[Status],
        value_x: str,
        model_y: type[Telemetry] | type[Status],
        value_y: str,
        flight_id: str | uuid.UUID,
        filtered_times: Iterable[datetime] | None = None,
    ) -> None:
        self.model_x = model_x
        self.value_x = value_x
        self.model_y = model_y
        self.value_y = value_y
        self.flight_id = flight_id
        self.filtered_times = filtered_times

    def run(self) -> float:
        """
        It computes correlation from two selected model fields.
        If the model is 'Telemetry' and the field is 'time_received', it automatically computes
        the difference between recorded time and received time.
        """
        get_data_chain: list[Any] = [
            self.get_model_data_by_time_received,
            self.get_velocity,
            self.get_haversine,
            self.get_model_data_by_value,
        ]
        data_x = self.chainer(get_data_chain, self.model_x, self.value_x)
        data_y = self.chainer(get_data_chain, self.model_y, self.value_y)

        if data_x is None or data_y is None:
            raise ImproperlyConfiguredFile('One or both datasets are empty.')

        if self.value_x == 'latitude longitude':
            data_y = data_y[1:]
        elif self.value_y == 'latitude longitude':
            data_x = data_x[1:]
        if len(data_x) != len(data_y):
            raise DifferentLengthException('Datasets have different length. Impossible to correlate them.')

        x = self.convert_data_to_numpy(data_x)
        y = self.convert_data_to_numpy(data_y)
        return abs(np.corrcoef(x, y))[0][1]

    @staticmethod
    def chainer(funcs: list[GetValue], *args: Any, **kwargs: Any) -> Any:
        """
        Handler of the chain of responsibility. `funcs` is a list methods to process.
        If `None`, other function is called or returns results.
        """
        result: Any = None
        for f in funcs:
            result = f(*args, **kwargs)
            if result is not None:
                return result
        return None

    def get_model_data_by_value(
        self,
        model: Telemetry | Status,
        value: str,
    ) -> QuerySet[Telemetry | Status] | None:
        """
        Returns a Django ORM query set of a selected field of a selected model.
        It expects only Telemetry or Status model. However, it can deal with any model if a time field is presented.
        Optionally filtered by dates is available.
        """
        if self.filtered_times:
            data = model.objects.filter(flight_id=self.flight_id, time__in=self.filtered_times).values_list(
                f'{value}', flat=True
            )
        else:
            data = model.objects.filter(flight_id=self.flight_id).values_list(f'{value}', flat=True)
        return data

    def get_model_data_by_time_received(
        self,
        model: Telemetry | Status,
        value: str,
    ) -> QuerySet[Telemetry | Status] | None:
        """
        Returns a Django ORM query set of a difference between time recorded and time received.
        It is computed from the Telemetry model.
        """
        if model == Telemetry and value == 'time_received' and self.filtered_times:  # type: ignore[comparison-overlap]
            data = (
                Telemetry.objects.filter(flight_id=self.flight_id, time__in=self.filtered_times)
                .annotate(time_delay=(MilliToMicro(EpochWithMilliseconds(F('time_received')) - Epoch(F('time')))))
                .values_list('time_delay', flat=True)
            )
            return data
        return None

    @staticmethod
    def compute_haversine_per_time(
        coor_a: tuple[float, float],
        coor_b: tuple[float, float],
        time_a: datetime,
        time_b: datetime,
        unit: haversine.Unit = haversine.Unit.METERS,
    ) -> float:
        """Computes velocity from coordinates and time."""
        haver = haversine.haversine(coor_a, coor_b, unit=unit)
        diff_time = datetime.timestamp(time_a) - datetime.timestamp(time_b)
        if not diff_time:
            raise SameTimeException
        return haver / diff_time

    def get_haversine(
        self,
        model: Telemetry | Status,
        value: str,
    ) -> list[float] | None:
        if model == Telemetry and value == 'latitude longitude':  # type: ignore[comparison-overlap]
            value_list = value.split()
            data = model.objects.filter(flight_id=self.flight_id).values(*value_list, 'time')

            coor_speed: list[float] = []
            previous_lat = previous_lon = previous_time = None
            for d in data:
                if previous_time:
                    coor = self.compute_haversine_per_time(  # type: ignore[unreachable]
                        (previous_lat, previous_lon),
                        (d['latitude'], d['longitude']),
                        previous_time,
                        d['time'],
                    )
                    coor_speed.append(coor)
                previous_lat = d['latitude']
                previous_lon = d['longitude']
                previous_time = d['time']
            return coor_speed
        return None

    def get_velocity(
        self,
        model: Telemetry | Status,
        value: str,
    ) -> list[float] | None:
        if model == Telemetry and value == 'velocity_x velocity_y':  # type: ignore[comparison-overlap]
            value_list = value.split()
            data = model.objects.filter(flight_id=self.flight_id).values(*value_list)

            vel_speed: list[float] = []
            for d in data:
                vel = math.sqrt(d['velocity_x'] ** 2 + d['velocity_y'] ** 2)
                vel_speed.append(vel)
            return vel_speed
        return None

    @staticmethod
    def convert_data_to_numpy(data: QuerySet[Any] | Sequence[Any]) -> npt.NDArray[np.float64]:
        """Converts sequences to numpy datatype."""
        if isinstance(data[0], datetime):
            data = [datetime.timestamp(d) for d in data]
        return np.array(data).astype('float64')


def get_chart_data(flight_id: uuid.UUID) -> QuerySet[Any]:
    data = (
        Status.objects.filter(flight_id=flight_id, time=F('flight_id__telemetries__time'))
        .annotate(
            time_delay=EpochWithMilliseconds(
                F('flight_id__telemetries__time_received')) - Epoch(F('flight_id__telemetries__time'))  # fmt: skip
        )
        .values('time', 'time_delay', 'rsrp', 'rsrq', 'snr')
    )
    return data
