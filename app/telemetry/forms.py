from django import forms


class UploadFileForm(forms.Form):
    telemetry_file = forms.FileField()
    status_file = forms.FileField()
