class BaseTelemetryException(Exception):  # noqa: N818
    pass


class SameTimeException(BaseTelemetryException):  # noqa: N818
    def __str__(self) -> str:
        return "You are not allowed to define identical times to compute time difference."


class UnsuitableModelException(BaseTelemetryException):  # noqa: N818
    pass


class ImproperlyConfiguredFile(BaseTelemetryException):  # noqa: N818
    pass


class DifferentLengthException(BaseTelemetryException):  # noqa: N818
    pass


class DuplicityFlightError(BaseTelemetryException):  # noqa: N818
    pass
