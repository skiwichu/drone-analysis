import uuid

from django.core.validators import MaxValueValidator, MinLengthValidator, MinValueValidator
from django.db import models
from django.urls import reverse


class Flight(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    date = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    takeoff_altitude = models.DecimalField(
        max_digits=7,
        decimal_places=2,
        validators=[MinValueValidator(-450), MaxValueValidator(10000)],
        null=True,
        blank=True,
        default=None,
    )

    def __str__(self) -> str:
        return f"{self.date:%Y-%m-%d %H:%M}: {self.id}"

    def get_absolute_url(self) -> str:
        return reverse('flight-detail', args=[str(self.id)])


class Telemetry(models.Model):
    time = models.DateTimeField()
    time_received = models.DateTimeField()
    latitude = models.FloatField(validators=[MinValueValidator(-90), MaxValueValidator(90)])
    longitude = models.FloatField(validators=[MinValueValidator(-180), MaxValueValidator(180)])
    altitude = models.DecimalField(
        max_digits=7,
        decimal_places=2,
        validators=[MinValueValidator(-450), MaxValueValidator(10000)],
    )
    geo_altitude = models.DecimalField(
        max_digits=6,
        decimal_places=1,
        validators=[MinValueValidator(-450), MaxValueValidator(10000)],
    )
    velocity_x = models.DecimalField(max_digits=4, decimal_places=1)
    velocity_y = models.DecimalField(max_digits=4, decimal_places=1)
    velocity_z = models.DecimalField(max_digits=4, decimal_places=1)
    horizontal_accuracy = models.SmallIntegerField()
    vertical_accuracy = models.SmallIntegerField()
    speed_accuracy = models.SmallIntegerField()
    pressure = models.FloatField(validators=[MinValueValidator(800), MaxValueValidator(1100)])
    flight_id = models.ForeignKey(Flight, on_delete=models.CASCADE, related_name='telemetries')

    class Meta:
        verbose_name_plural = 'telemetries'

    def __str__(self) -> str:
        return f"{self.flight_id.id}: {self.time:%Y-%m-%d %H:%M}"


class Status(models.Model):
    time = models.DateTimeField()
    time_received = models.DateTimeField()
    battery = models.FloatField()
    charging = models.BooleanField()
    satellites = models.PositiveSmallIntegerField()
    cellid = models.CharField(max_length=8, validators=[MinLengthValidator(8)])
    tac = models.CharField(max_length=4, validators=[MinLengthValidator(4)])
    rsrp = models.SmallIntegerField()
    rsrq = models.SmallIntegerField()
    snr = models.SmallIntegerField()
    flight_id = models.ForeignKey(Flight, on_delete=models.CASCADE, related_name='statuses')

    class Meta:
        verbose_name_plural = 'statuses'

    def __str__(self) -> str:
        return f"{self.flight_id.id}: {self.time:%Y-%m-%d %H:%M}"
