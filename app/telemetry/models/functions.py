from typing import Any

from django.db.backends.base.base import BaseDatabaseWrapper
from django.db.models import FloatField, IntegerField
from django.db.models.expressions import Func
from django.db.models.sql.compiler import SQLCompiler


class Epoch(Func):
    function: str = 'unixepoch'
    template: str = ''
    output_field = IntegerField()

    def as_sqlite(
        self, compiler: SQLCompiler, connection: BaseDatabaseWrapper, **extra_context: Any
    ) -> tuple[str, list[str | int]]:
        self.template = "%(function)s(%(expressions)s)"
        return super().as_sql(compiler, connection, **extra_context)


class EpochWithMilliseconds(Func):
    template: str = ''
    output_field = FloatField()

    def as_sqlite(
        self, compiler: SQLCompiler, connection: BaseDatabaseWrapper, **extra_context: Any
    ) -> tuple[str, list[str | int]]:
        self.template = "unixepoch(%(expressions)s) + mod(strftime('%%%%f', %(expressions)s), 1)"
        return super().as_sql(compiler, connection, **extra_context)


class MilliToMicro(Func):
    template: str = "1000000 * %(expressions)s"
    output_field = FloatField()
