import abc
import csv
import io
import sys
import uuid
from collections.abc import Iterator
from datetime import datetime
from decimal import Decimal
from typing import Any, NamedTuple

from django.core.management.base import OutputWrapper

from .exceptions import ImproperlyConfiguredFile


class TelemetrySchemaFile(NamedTuple):
    id: int
    time: datetime
    time_received: datetime
    latitude: float
    longitude: float
    altitude: Decimal
    geo_altitude: Decimal
    height: Decimal
    velocity_x: Decimal
    velocity_y: Decimal
    velocity_z: Decimal
    vertical_accuracy: int
    horizontal_accuracy: int
    speed_accuracy: int
    pressure: float


class StatusSchemaFile(NamedTuple):
    id: int
    time: datetime
    time_received: datetime
    battery: float
    cellid: str
    rsrp: int
    rsrq: int
    snr: int
    tac: str
    satellites: int
    charging: bool
    flight_id: uuid.UUID


class InputFileGenerator(abc.ABC):
    def __init__(self, path: str, schema: type[NamedTuple]) -> None:
        self.path = path
        self.schema = schema
        self.stdout = OutputWrapper(sys.stdout)
        self.stderr = OutputWrapper(sys.stderr)

    @abc.abstractmethod
    def iterate_file(
        self, path: str | None = None, schema: type[NamedTuple] | None = None
    ) -> Iterator[dict[str, Any]]:
        ...


class CSVFileGenerator(InputFileGenerator):
    def iterate_file(
        self, path: str | None = None, schema: type[NamedTuple] | None = None
    ) -> Iterator[dict[str, Any]]:
        if not path:
            path = self.path
        if not schema:
            schema = self.schema

        with open(path) as file:
            self.validate_csv_format(file)

            reader: csv.DictReader[Any] = csv.DictReader(file, fieldnames=schema._fields)
            header = tuple(next(reader).values())
            if schema._fields != header:
                raise ImproperlyConfiguredFile('Selected file does not have valid columns.')
            yield from reader

    @staticmethod
    def validate_csv_format(file: io.TextIOWrapper) -> int:
        try:
            csv.Sniffer().sniff(file.read(20))
        except (UnicodeDecodeError, TypeError, csv.Error):
            raise ImproperlyConfiguredFile('Selected file is not a csv format.')
        return file.seek(0)
