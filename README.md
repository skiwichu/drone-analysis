# Drones analysis - *test project*

This is a test project to demostrate programming abilities of me.

## About TODO:

The drones analysis project is a very simple application written in latest Django web-framework. The app follows twelve app methodology. Django covers the full-stack scope. It means there is no javascript library or framework on the front-end.

Check live version at <https://drones.skiwichu.eu>.

## Usage

### Build locally

`install.sh` should run at any Linux platform. Bellow is an example how to build a project on Debian-like system with suggested prerequisities. Plase follow steps in `install.sh` if you use a different Linux platform and consider installing appropriate dependecies. I cannot guarantee it works properly if your platform is Mac or Windows. If Windows, WSL is strongly advised.
``` sh
$ apt install python3 python3-venv python3-pip docker-ce xdg-utils
$ git clone https://gitlab.com/skiwichu/drones-analysis.git
$ ./install.sh
```

### Live website

Follow: <http://localhost:8088>

## Testing

There are two ways of testing. `pre-commit` is a git hook which is configured to run when `git commit` is pushed. You can find linters, formatters, and other checks in it. However, you can manually run it when you type:
``` sh
$ pre-commit run -a
```

To run tests and type-checking write down:
``` sh
$ docker compose exec web python manage.py test --mypy
```
